
const Umzug = require('umzug');

module.exports = (sequelize, appPath) => {
  const config = require(`${appPath}/config`);
  return new Umzug({
    storage: 'sequelize',
    storageOptions: {
      sequelize
    },

    migrations: {
      params: [
        sequelize.getQueryInterface(), // queryInterface
        sequelize.constructor, // DataTypes
        config.models,
        () => {
          throw new Error('Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.');
        }
      ],
      path: `${appPath}/database/migrations`,
      pattern: /\.js$/
    },

    logging: false
  });
};
