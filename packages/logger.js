/*jshint esversion: 6 */
const winston = require('winston');
const defaults = require('lodash/defaults');

module.exports = function (config) {
  let transports = [];

  if (config.log.console) {
    let consoleOptions = defaults(config.log.console, {
      level: config.log.console.level || 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    });

    transports.push(new winston.transports.Console(consoleOptions));
  }

  if (config.log.file) {
    transports.push(new winston.transports.File(defaults(config.log.file, {
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
      maxsize: 50000000000
    })));
  }

  let logger = new winston.createLogger({
    transports: transports
  });

  return {
    logger: logger,
    stream: (opt) => ({
      write: (log, encoding) => {
        opt = opt || {};
        logger.info(`methodName:${opt.methodName} requestId:${opt.id} `.concat(log));
      }
    })
  };
};