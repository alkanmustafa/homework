const express = require('express');

const router = express.Router();
const responser = require('../../../packages/responser');

router.get('/', async (req, res, next) => {
  return responser.success(res, { status: 'success' });
});

module.exports = router;
