
function InvalidEmail() {
  this.name = 'InvalidEmail';
  this.message = 'Invalid email';
  this.status = 406;
}
InvalidEmail.prototype = Error.prototype;
module.exports.InvalidEmail = InvalidEmail;

function UserNotActivated() {
  this.name = 'UserNotActivated';
  this.message = 'User not activated';
  this.status = 500;
}

UserNotActivated.prototype = Error.prototype;
module.exports.UserNotActivated = UserNotActivated;

function InvalidEmailOrPassword() {
  this.name = 'InvalidEmailOrPassword';
  this.message = 'Invalid email or password';
  this.status = 406;
}

InvalidEmailOrPassword.prototype = Error.prototype;
module.exports.InvalidEmailOrPassword = InvalidEmailOrPassword;
