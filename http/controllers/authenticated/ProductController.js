const express = require('express');
const router = express.Router();

const paginator = require('../../../packages/express-paginator').init('sequelize');

const responser = require('../../../packages/responser');
const ServiceFactory = require('../../../services/factory/ProductServiceFactory');
const ProductValidator = require('../../validators/ProductValidator');

router.get('/', [paginator.paginate], async (req, res, next) => {
  try {
    const products = await ServiceFactory.init().getProducts(req.paginator);
    return responser.success(res, products);
  } catch (error) {
    return responser.withError(next, error);
  }
});

router.get('/:productId', [ProductValidator.checkProductId], async (req, res, next) => {
  try {
    const product = await ServiceFactory.init().getProduct(req.params.productId);
    return responser.success(res, product);
  } catch (error) {
    return responser.withError(next, error);
  }
});

router.post('/', [ProductValidator.createProduct], async (req, res, next) => {
  try {
    const product = await ServiceFactory.init().create(req.body);
    return responser.success(res, product);
  } catch (error) {
    return responser.withError(next, error);
  }
});

router.delete('/:productId', [ProductValidator.checkProductId], async (req, res, next) => {
  try {
    const product = await ServiceFactory.init().delete(req.params.productId);
    return responser.success(res, product);
  } catch (error) {
    return responser.withError(next, error);
  }
});

router.post('/:productId/variant', [ProductValidator.createVariant], async (req, res, next) => {
  try {
    const product = await ServiceFactory.init().createVariant({
      colorId: req.body.colorId,
      sizeId: req.body.sizeId,
      quantity: req.body.quantity,
      productId: req.params.productId
    });
    return responser.success(res, product);
  } catch (error) {
    return responser.withError(next, error);
  }
});

router.put('/:productId/variant/:variantId', [ProductValidator.createVariant, ProductValidator.checkVariantId], async (req, res, next) => {
  try {
    const variant = await ServiceFactory.init().updateVariant({
      id: req.params.variantId,
      colorId: req.body.colorId,
      sizeId: req.body.sizeId,
      quantity: req.body.quantity,
      productId: req.params.productId
    });
    return responser.success(res, variant);
  } catch (error) {
    return responser.withError(next, error);
  }
});

router.delete('/:productId/variant/:variantId', [ProductValidator.checkProductId, ProductValidator.checkVariantId], async (req, res, next) => {
  try {
    const variant = await ServiceFactory.init().deleteVariant(req.params.productId, req.params.variantId);
    return responser.success(res, variant);
  } catch (error) {
    return responser.withError(next, error);
  }
});


module.exports = router;
