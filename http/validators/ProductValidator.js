const { isEmpty } = require('lodash');
const constant = require('../../constants/constant');
const { ValidationError } = require('../../errors/SystemErrors');
const ValidationFunctions = (req) => {
  return {
    product: async () => {
      const validateObject = {
        name: {
          notEmpty: true,
        },
        status: {
          notEmpty: true,
          matches: {
            options: [`^${constant.Status.Active}$|^${constant.Status.Inactive}$`],
            errorMessage: 'Invalid status identifier'
          }
        },
        price: {
          notEmpty: true,
          isFloat: {
            errorMessage: 'Invalid price identifier'
          }
        },
        images: {
          notEmpty: true
        }
      };
      req.checkBody(validateObject);

      const validateResult = await req.getValidationResult();
      if (!validateResult.isEmpty()) {
        throw new ValidationError(validateResult);
      }
      return null;
    },
    images: async () => {
      if (!Array.isArray(req.body.images)) {
        throw new ValidationError([{
          location: 'body',
          param: 'images',
          msg: 'images is array'
        }]);
      }
      const validateObject = {};
      if (req.body.images) {
        req.body.images.forEach((image, index) => {
          validateObject[`images[${index}].src`] = {
            notEmpty: true
          };
        });
      }
      req.checkBody(validateObject);

      const validateResult = await req.getValidationResult();
      if (!validateResult.isEmpty()) {
        throw new ValidationError(validateResult);
      }
      return null;
    },
    color: async () => {
      if (!isEmpty(req.body.colors)) {
        if (!Array.isArray(req.body.colors)) {
          throw new ValidationError([{
            location: 'body',
            param: 'colors',
            msg: 'colors is array'
          }]);
        }
        const validateObject = {};
        if (req.body.colors) {
          req.body.colors.forEach((image, index) => {
            validateObject[`colors[${index}].value`] = {
              notEmpty: true
            };
          });
        }
        req.checkBody(validateObject);

        const validateResult = await req.getValidationResult();
        if (!validateResult.isEmpty()) {
          throw new ValidationError(validateResult);
        }
      }
      return null;
    },
    size: async () => {
      if (!isEmpty(req.body.sizes)) {
        if (!Array.isArray(req.body.sizes)) {
          throw new ValidationError([{
            location: 'body',
            param: 'sizes',
            msg: 'sizes is array'
          }]);
        }
        const validateObject = {};
        if (req.body.sizes) {
          req.body.sizes.forEach((image, index) => {
            validateObject[`sizes[${index}].value`] = {
              notEmpty: true
            };
          });
        }
        req.checkBody(validateObject);

        const validateResult = await req.getValidationResult();
        if (!validateResult.isEmpty()) {
          throw new ValidationError(validateResult);
        }
      }
      return null;
    },
    paramProductId: async () => {
      req.checkParams({
        productId: {
          notEmpty: true,
          isInt: {
            errorMessage: 'Invalid productId identifier'
          }
        },
      });

      const validateResult = await req.getValidationResult();
      if (!validateResult.isEmpty()) {
        throw new ValidationError(validateResult);
      }
      return null;
    },
    paramVariantId: async () => {
      req.checkParams({
        variantId: {
          notEmpty: true,
          isInt: {
            errorMessage: 'Invalid variantId identifier'
          }
        },
      });

      const validateResult = await req.getValidationResult();
      if (!validateResult.isEmpty()) {
        throw new ValidationError(validateResult);
      }
      return null;
    },
    variant: async () => {
      req.checkBody({
        quantity: {
          notEmpty: true,
          isInt: {
            errorMessage: 'Invalid quantity identifier'
          }
        },
      });
      const validateResult = await req.getValidationResult();
      if (!validateResult.isEmpty()) {
        throw new ValidationError(validateResult);
      }
      return null;
    }
  }
};
const ProductValidator = {};

ProductValidator.createProduct = async (req, res, next) => {
  const validateFunctions = ValidationFunctions(req, res, next);
  try {
    await validateFunctions.product();
    await validateFunctions.images();
    await validateFunctions.color();
    await validateFunctions.size();
    return next();
  } catch (error) {
    return next(error);
  }
};
ProductValidator.createVariant = async (req, res, next) => {
  const validateFunctions = ValidationFunctions(req, res, next);
  try {
    await validateFunctions.paramProductId();
    await validateFunctions.variant();
    return next();
  } catch (error) {
    return next(error);
  }
};

ProductValidator.checkProductId = async (req, res, next) => {
  const validateFunctions = ValidationFunctions(req, res, next);
  try {
    await validateFunctions.paramProductId();
    return next();
  } catch (error) {
    return next(error);
  }
};
ProductValidator.checkVariantId = async (req, res, next) => {
  const validateFunctions = ValidationFunctions(req, res, next);
  try {
    await validateFunctions.paramVariantId();
    return next();
  } catch (error) {
    return next(error);
  }
};

module.exports = ProductValidator;

