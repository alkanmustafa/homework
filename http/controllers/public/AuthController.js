const express = require('express');

const router = express.Router();
const responser = require('../../../packages/responser');

const AuthValidator = require('../../validators/AuthValidator');
const ServiceFactory = require('../../../services/factory/AuthServiceFactory');

router.post('/login', [AuthValidator.postAuth], async (req, res, next) => {
  try {
    const token = await ServiceFactory.init().auth(req.body.email, req.body.password);
    return responser.success(res, { token });
  } catch (error) {
    return responser.withError(next, error);
  }
});

module.exports = router;
