module.exports = {
  up(query, DataTypes, modelPath) {
    const Model = require(`${modelPath}/User`)(query.sequelize, DataTypes);
    if (this.seed) {
      return Model.sync().then(() => this.seed(query, DataTypes, modelPath));
    }
    return Model.sync();
  },

  down(query, DataTypes, modelPath) {
    const Model = require(`${modelPath}/User`)(query.sequelize, DataTypes);
    return query.dropTable(Model.getTableName());
  },
  seed(query, DataTypes, modelPath) {
    const Model = require(`${modelPath}/User`)(query.sequelize, DataTypes);

    return Model.bulkCreate([
      {
        name: 'Mustafa ALKAN',
        email: 'mralkanmustafa@gmail.com',
        password: '123qwe',
        status: 'Active'
      }
    ]);
  }
};
