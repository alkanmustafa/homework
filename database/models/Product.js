const constant = require('../../constants/constant');

module.exports = (sequelize, DataTypes) => {
  const ProductModel = sequelize.define(
    'Product',
    {
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
        field: 'name'
      },
      slug: {
        type: DataTypes.STRING(255),
        allowNull: false,
        field: 'slug'
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true,
        field: 'description'
      },
      status: {
        type: DataTypes.ENUM(constant.Status.Active, constant.Status.Inactive),
        defaultValue: constant.Status.Active,
        field: 'status'
      },
      price: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false,
        field: 'price'
      },
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
        field: 'quantity'
      }
    }, {
    tableName: 'products',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [{
      unique: true,
      fields: ['slug']
    }],
    paranoid: true
  }
  );

  ProductModel.associate = (models) => {
    ProductModel.hasMany(models.ProductImage, {
      foreignKey: 'productId'
    });

    ProductModel.hasMany(models.ProductColor, {
      foreignKey: 'productId'
    });

    ProductModel.hasMany(models.ProductSize, {
      foreignKey: 'productId'
    });

    ProductModel.hasMany(models.ProductVariant, {
      foreignKey: 'productId'
    });
  };

  return ProductModel;
};
