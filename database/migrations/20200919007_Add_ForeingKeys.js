const getQuery = (query, DataTypes, modelPath, status = 'up') => {
  let queries = [];
  if (status === 'up') {
    queries = [
      'ALTER TABLE `product_colors` ADD CONSTRAINT fk_product_id_color FOREIGN KEY (product_id) REFERENCES products(id);',
      'ALTER TABLE `product_images` ADD CONSTRAINT fk_product_id_images FOREIGN KEY (product_id) REFERENCES products(id);',
      'ALTER TABLE `product_sizes` ADD CONSTRAINT fk_product_id_size FOREIGN KEY (product_id) REFERENCES products(id);',
      'ALTER TABLE `product_variants` ADD CONSTRAINT fk_product_id_variant FOREIGN KEY (product_id) REFERENCES products(id);',
    ];
  } else if (status === 'down') {
    queries = [
      'ALTER TABLE `product_colors` DROP FOREIGN KEY fk_product_id_color;',
      'ALTER TABLE `product_images` DROP FOREIGN KEY fk_product_id_images;',
      'ALTER TABLE `product_sizes` DROP FOREIGN KEY fk_product_id_size;',
      'ALTER TABLE `product_variants` DROP FOREIGN KEY fk_product_id_variant;',
      'ALTER TABLE`product_colors` DROP INDEX fk_product_id_color; ',
      'ALTER TABLE `product_images` DROP INDEX fk_product_id_images;',
      'ALTER TABLE `product_sizes` DROP INDEX fk_product_id_size;',
      'ALTER TABLE `product_variants` DROP INDEX fk_product_id_variant;',
    ];
  } else {
    throw new Error('[DEVELOPER ERROR] invalid query status');
  }
  return queries;
};
module.exports = {
  up: (query, DataTypes) => query.sequelize.query(getQuery(query, DataTypes, null, 'up').join(''), { raw: true }),
  down: (query, DataTypes) => query.sequelize.query(getQuery(query, DataTypes, null, 'down').join(''), { raw: true }),
  getQuery
};