const constant = require('../../constants/constant');
const hashHelper = require('../../packages/hash');

module.exports = (sequelize, DataTypes) => {
  const UserModel = sequelize.define(
    'User',
    {
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
        field: 'name'
      },
      email: {
        type: DataTypes.STRING(50),
        allowNull: false,
        field: 'email'
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'password',
        set(pass) {
          this.setDataValue('password', hashHelper.bcryptSync(pass));
        }
      },
      status: {
        type: DataTypes.ENUM(constant.Status.Active, constant.Status.Inactive),
        defaultValue: constant.Status.Active,
        field: 'status'
      }
    }, {
    tableName: 'users',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [{
      unique: true,
      fields: ['email']
    }],
    paranoid: true
  }
  );

  UserModel.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());
    delete values.password;
    delete values.activationCode;
    delete values.forgotPasswordCode;
    delete values.role;
    return values;
  };

  return UserModel;
};
