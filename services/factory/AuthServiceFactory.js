const db = require('../../database/connection');
const AuthService = require('../AuthService');

module.exports.init = () => {
	return AuthService(db);
};
