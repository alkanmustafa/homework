module.exports = {
  up(query, DataTypes, modelPath) {
    const Model = require(`${modelPath}/Product`)(query.sequelize, DataTypes);
    if (this.seed) {
      return Model.sync().then(() => this.seed(query, DataTypes, modelPath));
    }
    return Model.sync();
  },

  down(query, DataTypes, modelPath) {
    const Model = require(`${modelPath}/Product`)(query.sequelize, DataTypes);
    return query.dropTable(Model.getTableName());
  }
};
