const slugify = require('slugify');
const { pick, assignIn } = require('lodash');
const ProductErrors = require('../errors/ProductErrors');

module.exports = (db) => {
  const ProductService = {};

  ProductService.create = async (data) => {
    data.slug = slugify(data.name, {
      replacement: '-',
      lower: true,
      strict: true,
      locale: 'en'
    });
    const isExist = await db.Product.findOne({ where: { slug: data.slug } });
    if (isExist) {
      throw new ProductErrors.ProductExist();
    }
    const product = await db.Product.create(pick(data, ['name', 'status', 'price', 'slug']));
    await ProductService.createImages(product.id, data.images);
    await ProductService.createColors(product.id, data.colors);
    await ProductService.createSizes(product.id, data.sizes);
    return await ProductService.getProduct(product.id);
  };

  ProductService.createVariant = async ({ productId, quantity, colorId = null, sizeId = null }) => {
    const isExistVariant = await db.ProductVariant.findOne({ where: { productId, colorId, sizeId } });
    if (isExistVariant) {
      throw new ProductErrors.VariantIsExist();
    }
    await db.ProductVariant.create({
      productId,
      quantity,
      colorId,
      sizeId
    });
    return ProductService.manageQuantity(productId);
  };

  ProductService.createImages = async (productId, images = []) => {
    images.map((image) => {
      image.productId = productId;
    });
    return await db.ProductImage.bulkCreate(images);
  };

  ProductService.createColors = async (productId, colors = []) => {
    if (!colors.length) {
      return colors;
    }
    colors.map((color) => {
      color.productId = productId;
    });
    return await db.ProductColor.bulkCreate(colors);
  };

  ProductService.createSizes = async (productId, sizes = []) => {
    if (!sizes.length) {
      return sizes;
    }
    sizes.map((size) => {
      size.productId = productId;
    });
    return await db.ProductSize.bulkCreate(sizes);
  };

  ProductService.manageQuantity = async (productId) => {
    const product = await db.Product.findByPk(productId);
    if (!product) {
      throw new ProductErrors.NotFound();
    }
    const variants = await db.ProductVariant.findAll({ where: { productId } });
    let quantity = 0;

    if (variants.length) {
      variants.forEach((variant) => {
        quantity += variant.quantity
      });
    }
    product.quantity = quantity;
    await product.save();
    return ProductService.getProduct(productId);
  }

  ProductService.getProduct = async (productId) => {
    const queryOptions = {
      include: [
        {
          model: db.ProductImage,
          required: false
        },
        {
          model: db.ProductVariant,
          required: false,
          include: [
            {
              model: db.ProductColor,
              required: false
            },
            {
              model: db.ProductSize,
              required: false
            }
          ]
        },
        {
          model: db.ProductColor,
          required: false
        },
        {
          model: db.ProductSize,
          required: false
        }
      ]
    };
    const product = await db.Product.findByPk(productId, queryOptions);
    if (!product) {
      throw new ProductErrors.ProductNotFound();
    }

    return product;
  };

  ProductService.delete = async (productId) => {
    const product = await db.Product.findByPk(productId);
    await product.destroy();
    await db.ProductImage.destroy({ where: { productId } });
    await db.ProductColor.destroy({ where: { productId } });
    await db.ProductSize.destroy({ where: { productId } });
    await db.ProductVariant.destroy({ where: { productId } });
    return null;
  }

  ProductService.getProducts = async (pagination) => {
    const count = await db.Product.count(pagination);
    if (!count) {
      throw new ProductErrors.ProductNotFound();
    }
    const options = assignIn(pagination, {
      include: [
        {
          model: db.ProductImage,
          required: false
        },
        {
          model: db.ProductVariant,
          required: false,
          include: [
            {
              model: db.ProductColor,
              required: false
            },
            {
              model: db.ProductSize,
              required: false
            }
          ]
        },
        {
          model: db.ProductColor,
          required: false
        },
        {
          model: db.ProductSize,
          required: false
        }
      ]
    });
    const products = await db.Product.findAll(options);
    return products;
  }

  ProductService.updateVariant = async ({ id, productId, quantity, colorId = null, sizeId = null }) => {
    const variant = await db.ProductVariant.findByPk(id);
    variant.productId = productId || variant.productId;
    variant.quantity = quantity || variant.quantity;
    variant.colorId = colorId || variant.colorId;
    variant.sizeId = sizeId || variant.sizeId;
    await variant.save();
    return ProductService.manageQuantity(productId);
  };

  ProductService.deleteVariant = async (productId, variantId) => {
    const variant = await db.ProductVariant.findByPk(variantId);
    await variant.destroy();
    return ProductService.manageQuantity(productId);
  }

  return ProductService;
};
