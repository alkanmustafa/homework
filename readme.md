# Homework

Beceri Değerlendirme Projesi 

## Gereksinimler

```bash
1- Node@12
2- Mysql@5.7
```

## Kurulum

```bash
1) git clone https://alkanmustafa@bitbucket.org/alkanmustafa/homework.git

2) cd homework (ya da proje adı)

3) npm install (--production)

4) copy config-example.js to config.js

5) edit config.js configurations (database, auth.secretKey)

6) cd ${project_path}/database/migrator

7) node migrate.js environmentUp (ya da manuel import export.sql)

8) cd ${project_path} && node bin/www

9) Postman Import & Select Env
   homework.postman_environment.json
   Homework.postman_collection.json
```

## Kullanım

1) Select postman env "homework"

2) Run postman collection requests Operating Order (Recommended)  
-Test-Authenticated-Url  
-Login  
-Test-Authenticated-Url  
-Create Product  
-Create Product Variant - 1  
-Create Product Variant - 2  
-Create Product Variant - 3  
-Get Product By Id  
-Get Product List  
-Update Variant  
-Variant Delete   
-Get Product By Id or Get Product List  