const upperFirst = require("lodash/upperFirst");
const { Op } = require('sequelize');
module.exports.functions = () => {

    let Gt = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.gt"] = options.value;
        else
            options.criteria[options.key] = { [Op.gt]: options.value };

        return options.criteria;

    }

    let Gte = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.gte"] = options.value;
        else
            options.criteria[options.key] = { [Op.gte]: options.value };

        return options.criteria;
    }

    let Lt = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.lt"] = options.value;
        else
            options.criteria[options.key] = { [Op.lt]: options.value };

        return options.criteria;

    }

    let Lte = (options) => {
        if (options.criteria[options.key])
            options.criteria[options.key]["Op.lte"] = options.value;
        else
            options.criteria[options.key] = { [Op.lte]: options.value };

        return options.criteria;
    }

    let Ne = (options) => {
        if (options.criteria[options.key])
            options.criteria[options.key]["Op.ne"] = options.value;
        else
            options.criteria[options.key] = { [Op.ne]: options.value };

        return options.criteria;
    }

    let Eq = (options) => {
        if (options.criteria[options.key])
            options.criteria[options.key]["Op.eq"] = options.value;
        else
            options.criteria[options.key] = { [Op.eq]: options.value };

        return options.criteria;
    }

    let In = (options) => {
        if (options.criteria[options.key])
            options.criteria[options.key]["Op.in"] = options.value.split(',');
        else
            options.criteria[options.key] = { [Op.in]: options.value.split(',') };

        return options.criteria;
    }

    let NotIn = (options) => {
        if (options.criteria[options.key])
            options.criteria[options.key]["Op.notIn"] = options.value.split(',');
        else
            options.criteria[options.key] = { [Op.notIn]: options.value.split(',') };

        return options.criteria;
    }

    let Like = (options) => {

        options.value = '%' + options.value + '%';

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.like"] = options.value;
        else
            options.criteria[options.key] = { [Op.like]: options.value };

        return options.criteria;
    }

    let NotLike = (options) => {

        options.value = '%' + options.value + '%';

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.notLike"] = options.value;
        else
            options.criteria[options.key] = { [Op.notLike]: options.value };

        return options.criteria;
    }

    let Null = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.eq"] = null;
        else
            options.criteria[options.key] = { [Op.eq]: null };

        return options.criteria;
    }

    let NotNull = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.ne"] = null;
        else
            options.criteria[options.key] = { [Op.ne]: null };

        return options.criteria;
    }

    let Sw = (options) => {

        options.value = options.value + '%';

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.like"] = options.value;
        else
            options.criteria[options.key] = { [Op.like]: options.value };

        return options.criteria;
    }

    let Ew = (options) => {

        options.value = '%' + options.value;

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.like"] = options.value;
        else
            options.criteria[options.key] = { [Op.like]: options.value };

        return options.criteria;
    }

    let Between = (options) => {

        let splitedValue = options.value.split(',');

        if (splitedValue.length < 2 || splitedValue.length > 2)
            throw new Error('express-paginator validate: Invalid parameters: between value');

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.between"] = splitedValue;
        else
            options.criteria[options.key] = { [Op.between]: splitedValue };

        return options.criteria;

    }

    let NotBetween = (options) => {

        let splitedValue = options.value.split(',');

        if (splitedValue.length < 2 || splitedValue.length > 2)
            throw new Error('express-paginator validate: Invalid parameters: between value');

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.notBetween"] = splitedValue;
        else
            options.criteria[options.key] = { [Op.notBetween]: splitedValue };

        return options.criteria;

    }

    let IsTrue = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.eq"] = true;
        else
            options.criteria[options.key] = { [Op.eq]: true };

        return options.criteria;

    }

    let IsFalse = (options) => {

        if (options.criteria[options.key])
            options.criteria[options.key]["Op.eq"] = false;
        else
            options.criteria[options.key] = { [Op.eq]: false };

        return options.criteria;

    }

    return {
        Gt: Gt,
        Gte: Gte,
        Lt: Lt,
        Lte: Lte,
        Ne: Ne,
        Eq: Eq,
        In: In,
        NotIn: NotIn,
        Like: Like,
        NotLike: NotLike,
        Null: Null,
        NotNull: NotNull,
        Sw: Sw,
        Ew: Ew,
        Between: Between,
        NotBetween: NotBetween,
        IsTrue: IsTrue,
        IsFalse: IsFalse
    }
}

module.exports.paginate = (req) => {

    let paginate = {};

    if (req.query.sortType || req.query.sortBy || req.query.offset || req.query.limit) {

        paginate = {
            limit: parseInt(req.query.limit),
            offset: parseInt(req.query.offset),
            order: [req.query.sortBy, req.query.sortType],
        };

        if (req.query.sortBy.indexOf('.') >= 1) {

            let tmp = req.query.sortBy.split('.');

            paginate["order"] = [
                {
                    modelName: upperFirst(tmp[0]),
                    as: tmp[0]
                },
                tmp[1],
                req.query.sortType
            ];
        }

        delete req.query.limit;
        delete req.query.offset;
        delete req.query.sortBy;
        delete req.query.sortType;
    }

    req.paginator = req.paginator || {};
    req.paginator.paginate = paginate;

    return req;
}

module.exports.set = (req) => {

    let paginator = req.paginator;

    req.paginator = {
        limit: paginator.paginate.limit,
        offset: paginator.paginate.offset,
        order: paginator.paginate.order ? [paginator.paginate.order] : [],
        where: paginator.where,
        includedWhere: paginator.includedWhere || {},
    }
}