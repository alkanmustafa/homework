module.exports = (logger = {}) => (err, req, res, next) => {
  if (Object.keys(logger).length) {
    logger.info(`methodName:expressAppErrored requestId:${req.id} callStep:errored errorName:${err.name} errorStatus:${err.status} errorMessage:${err.message} errorStack:${err.stack} ${JSON.stringify(err)}`);
  }
  res.status(err.status || 500);
  const error = {
    message: err.message,
    error: err.meta || {},
    status: err.status || 500
  };
  if (err.data && Object.keys(err.data).length) {
    error.data = err.data;
  }
  res.json(error);
};
