
function ProductExist() {
  this.name = 'ProductExist';
  this.message = 'The product has been added before';
  this.status = 406;
}
ProductExist.prototype = Error.prototype;
module.exports.ProductExist = ProductExist;

function ProductNotFound() {
  this.name = 'ProductNotFound';
  this.message = 'The product not found';
  this.status = 404;
}
ProductNotFound.prototype = Error.prototype;
module.exports.ProductNotFound = ProductNotFound;

function VariantIsExist() {
  this.name = 'VariantIsExist';
  this.message = 'The product variant has been added before';
  this.status = 406;
}
VariantIsExist.prototype = Error.prototype;
module.exports.VariantIsExist = VariantIsExist;
