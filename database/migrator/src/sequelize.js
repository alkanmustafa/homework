const Sequelize = require('sequelize');

module.exports = (appPath) => {
  const config = require(`${appPath}/config`);
  return new Sequelize(config.database.database, config.database.user, config.database.password, {
    host: config.database.host,
    port: config.database.port,
    dialect: config.database.dialect,

    pool: {
      max: config.database.maxConnection || 5,
      min: 0,
      idle: 10000
    },

    dialectOptions: {
      multipleStatements: true
    }
  });
};
