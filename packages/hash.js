const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('../config');
const bcrypt = require('bcrypt');

module.exports.bcryptSync = (str) => {
  return bcrypt.hashSync(str, bcrypt.genSaltSync(3));
};

module.exports.bcryptCompareSync = (str, hash) => {
  return bcrypt.compareSync(str, hash);
};

module.exports.getJwtToken = (data, options = { expiresIn: config.auth.tokenExpiresIn }) => {
  return jwt.sign(data, config.auth.secretKey, options);
};

module.exports.verifyJwtToken = (token) => {
  return jwt.verify(token, config.auth.secretKey, { algorithms: ['HS256'] });
};
