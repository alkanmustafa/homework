module.exports = (sequelize, DataTypes) => {
  const ProductImageModel = sequelize.define(
    'ProductImage',
    {
      src: {
        type: DataTypes.STRING(255),
        allowNull: false,
        field: 'src'
      },
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: 'product_id'
      }
    }, {
    tableName: 'product_images',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    paranoid: true
  }
  );
  ProductImageModel.associate = (models) => {
    ProductImageModel.belongsTo(models.Product, {
      foreignKey: 'productId'
    });
  };
  return ProductImageModel;
};
