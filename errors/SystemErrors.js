function ValidationError(meta) {
  this.name = 'ValidationError';
  this.message = 'Validation Error';
  this.status = 406;
  this.meta = meta.array ? meta.array() : meta || {};
}

ValidationError.prototype = Error.prototype;
module.exports.ValidationError = ValidationError;

function UnauthorizedError() {
  this.name = 'UnauthorizedError';
  this.message = 'Unauthorized';
  this.status = 401;
}

UnauthorizedError.prototype = Error.prototype;
module.exports.UnauthorizedError = UnauthorizedError;