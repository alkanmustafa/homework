module.exports = (sequelize, DataTypes) => {
  const ProductVariantModel = sequelize.define(
    'ProductVariant',
    {
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: 'product_id'
      },
      colorId: {
        type: DataTypes.STRING(255),
        allowNull: true,
        field: 'color_id'
      },
      sizeId: {
        type: DataTypes.STRING(255),
        allowNull: true,
        field: 'size_id'
      },
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'quantity'
      },
    }, {
    tableName: 'product_variants',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [{
      unique: true,
      fields: ['product_id', 'color_id', 'size_id']
    }],
    paranoid: true
  }
  );
  ProductVariantModel.associate = (models) => {
    ProductVariantModel.belongsTo(models.Product, {
      foreignKey: 'productId'
    });
    ProductVariantModel.belongsTo(models.ProductSize, {
      foreignKey: 'sizeId'
    });
    ProductVariantModel.belongsTo(models.ProductColor, {
      foreignKey: 'colorId'
    });
  };
  return ProductVariantModel;
};
