
const path = require('path');

const appPath = path.normalize(`${__dirname}/../..`);

const functions = require('./src/factory')(appPath);

const cmd = process.argv[2].trim();
let executedCmd;

console.log(`${cmd.toUpperCase()} BEGIN`);

switch (cmd) {
  case 'query':
    executedCmd = functions.cmdShowQuery();
    break;

  case 'status':
    executedCmd = functions.cmdStatus();
    break;

  case 'environmentUp':
    executedCmd = functions.cmdMigrate();
    break;

  case 'next':
  case 'migrate-next':
    executedCmd = functions.cmdMigrateNext();
    break;

  case 'prev':
  case 'reset-prev':
    executedCmd = functions.cmdResetPrev();
    break;

  default:
    console.log(`invalid cmd: ${cmd}`);
    process.exit(1);
}

executedCmd
  .then((result) => {
    const doneStr = `${cmd.toUpperCase()} DONE`;
    console.log(doneStr);
    console.log('='.repeat(doneStr.length));
  })
  .catch((err) => {
    const errorStr = `${cmd.toUpperCase()} ERROR`;
    console.log(errorStr);
    console.log('='.repeat(errorStr.length));
    console.log(err);
    console.log('='.repeat(errorStr.length));
  })
  .then(() => {
    if (cmd !== 'status' && cmd !== 'reset-hard' && cmd !== 'query') {
      return functions.cmdStatus();
    }
    return Promise.resolve();
  })
  .then(() => process.exit(0));
