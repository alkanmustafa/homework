const chalk = require('chalk');
const path = require('path');
const isFunction = require('lodash/isFunction');
const fs = require('fs');
const { eachSeries } = require('async');

const { exec } = require('child_process');

const printSquare = (color) => {
  return {
    printLine: (message) => {
      console.log('*', chalk[color](message), ' '.repeat((process.stdout.columns) - (message.length % process.stdout.columns) - 5), '*');
    },
    printStart: () => {
      console.log('*'.repeat(process.stdout.columns));
    }
  };
};

module.exports = (umzug, sequelize, appPath) => {
  const config = require(`${appPath}/config`);
  const functions = {};

  functions.cmdStatus = (returnToStatus = false, printOutList = true) => {
    const result = {};

    return umzug.executed()
      .then((executed) => {
        result.executed = executed;
        return umzug.pending();
      }).then((pending) => {
        result.pending = pending;
        return result;
      }).then(({ executed, pending }) => {
        executed = executed.map((m) => {
          m.name = path.basename(m.file, '.js');
          return m;
        });
        pending = pending.map((m) => {
          m.name = path.basename(m.file, '.js');
          return m;
        });

        const current = executed.length > 0 ? executed[0].file : '<NO_MIGRATIONS>';
        const status = {
          current,
          executed: executed.map(m => m.file),
          pending: pending.map(m => m.file)
        };

        if (printOutList) { console.log(JSON.stringify(status, null, 2)); }

        if (returnToStatus) { return status; }
        return { executed, pending };
      });
  };

  functions.cmdShowQuery = () => {
    let status = {};

    return functions.cmdStatus(true, false).then((_status) => {
      status = _status;

      return functions.cmdShowPrevQuery(status);
    }).then((result) => {
      return functions.cmdShowNextQuery(status);
    });
  };

  functions.cmdMigrate = () => new Promise((res, rej) => {
    return functions.cmdStatus()
      .then((status) => {
        if (status.executed.length === 0) {
          return functions.envUp(status.pending);
        }
        throw new Error('Already environment avaible');
      })
      .then(res)
      .catch(rej);
  });

  functions.cmdMigrateNext = () => {
    return functions.cmdStatus().then(({ executed, pending }) => {
      if (pending.length === 0) {
        return Promise.reject(new Error('No pending migrations'));
      }
      const next = pending[0].name;
      return umzug.up({ to: next });
    });
  };

  functions.cmdResetPrev = () => {
    return functions.cmdStatus().then(({ executed, pending }) => {
      if (executed.length === 0) {
        return Promise.reject(new Error('Already at initial state'));
      }
      const prev = executed[executed.length - 1].name;
      return umzug.down({ to: prev });
    });
  };

  functions.cmdShowPrevQuery = (status) => {
    return new Promise((res, rej) => {
      if (status.executed.length > 0) {
        const migrationStatus = ['down', 'up'];
        const migrationFile = status.executed[status.executed.length - 1];
        const migrationModel = require(`${appPath}/database/migrations/${migrationFile}`);

        if (migrationModel.getQuery && isFunction(migrationModel.getQuery)) {
          console.log('\n ');
          printSquare().printStart();
          printSquare('green').printLine(`PREV MIGRATION - [${migrationFile}]`);

          migrationStatus.forEach((statusItem) => {
            const queries = migrationModel.getQuery(
              sequelize.getQueryInterface(),
              sequelize.constructor,
              config.models,
              statusItem
            );

            printSquare('blue').printLine(`${statusItem.toUpperCase()} Query :`);

            if (statusItem === 'down') {
              queries.forEach((queryItem) => {
                printSquare('red').printLine(queryItem);
              });
            } else {
              queries.forEach((queryItem) => {
                printSquare('gray').printLine(queryItem);
              });
            }

            printSquare('blue').printLine(`End ${statusItem.toUpperCase()} Query`);
          });

          printSquare('green').printLine(`PREV MIGRATION - [${migrationFile}]`);
          printSquare().printStart();
          console.log('\n ');
          return res(null);
        }
        return rej(new Error(`[DEVELOPER ERROR - PREV] ${migrationFile} query string not found`));
      }
      return rej(new Error('[DEVELOPER ERROR] - executed migration not found'));
    });
  };

  functions.cmdShowNextQuery = (status) => {
    return new Promise((res, rej) => {
      if (status.pending.length > 0) {
        const migrationStatus = ['up', 'down'];
        const migrationFile = status.pending[0];
        const migrationModel = require(`${appPath}/database/migrations/${migrationFile}`);

        if (migrationModel.getQuery && isFunction(migrationModel.getQuery)) {
          console.log('\n');
          printSquare().printStart();
          printSquare('green').printLine(`NEXT MIGRATION - [${migrationFile}]`);

          migrationStatus.forEach((statusItem) => {
            const queries = migrationModel.getQuery(
              sequelize.getQueryInterface(),
              sequelize.constructor,
              config.models,
              statusItem
            );

            printSquare('blue').printLine(`${statusItem.toUpperCase()} Query :`);

            if (statusItem === 'up') {
              queries.forEach((queryItem) => {
                printSquare('red').printLine(queryItem);
              });
            } else {
              queries.forEach((queryItem) => {
                printSquare('gray').printLine(queryItem);
              });
            }

            printSquare('blue').printLine(`End ${statusItem.toUpperCase()} Query`);
          });

          printSquare('green').printLine(`NEXT MIGRATION - [${migrationFile}]`);
          printSquare().printStart();
          console.log('\n');

          return res(null);
        }
        return rej(new Error(`[DEVELOPER ERROR - NEXT] ${migrationFile} query string not found`));
      }
      return rej(new Error('[DEVELOPER ERROR] - pending migration not found'));
    });
  };

  functions.envUp = (scripts) => new Promise(async (res, rej) => {
    eachSeries(scripts, (script, callback) => {
      functions.runCommand(`cd ${config.models} && cd .. && cd ./migrator && node migrate next`)
        .then(() => {
          return callback();
        })
        .catch((err) => {
          console.log(new Error(`[SYNC ERROR] - ${err}`));
          return callback(error);
        });
    }, (err) => {
      return res();
    });
  });

  functions.runCommand = (command) => new Promise((res, rej) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        return rej(err);
      } else {
        return res({
          stdout, stderr
        })
      }
    });
  });
  return functions;
};
