const { ValidationError } = require('../../errors/SystemErrors');

const AuthValidator = {};

AuthValidator.postAuth = (req, res, next) => {
  req.checkBody({
    email: {
      notEmpty: true,
      isEmail: { errorMessage: 'Invalid email' }
    },
    password: {
      notEmpty: true
    }
  });

  req
    .getValidationResult()
    .then((result) => {
      if (!result.isEmpty()) {
        return next(new ValidationError(result));
      }
      return next();
    })
    .catch(next);
};

module.exports = AuthValidator;

