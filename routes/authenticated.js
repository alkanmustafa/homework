module.exports = (app) => {
  app.use('/', require('../http/controllers/authenticated/TestController'));
  app.use('/products', require('../http/controllers/authenticated/ProductController'));
};
