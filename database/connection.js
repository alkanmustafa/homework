const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const config = require('../config');
const { logger } = require('../packages/logger')(config);

const db = {};
const sequelize = new Sequelize(config.database.database, config.database.user, config.database.password, {
  host: config.database.host,
  port: config.database.port,
  dialect: config.database.dialect,

  pool: {
    max: config.database.maxConnection || 5,
    min: 0,
    idle: 10000
  },

  dialectOptions: {
    multipleStatements: true
  },
  logging: (errorMessage) => {
    logger.debug(errorMessage);
  }
});

fs.readdirSync(config.models)
  .forEach((file) => {
    if (!_.endsWith(file, 'js')) {
      return;
    }

    const model = require(path.join(config.models, file))(sequelize, Sequelize.DataTypes)

    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
