module.exports = (sequelize, DataTypes) => {
  const ProductColorModel = sequelize.define(
    'ProductColor',
    {
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: 'product_id'
      },
      value: {
        type: DataTypes.STRING(255),
        allowNull: false,
        field: 'value'
      }
    }, {
    tableName: 'product_colors',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    paranoid: true
  }
  );

  ProductColorModel.associate = (models) => {
    ProductColorModel.belongsTo(models.Product, {
      foreignKey: 'productId'
    });
  };

  return ProductColorModel;
};
