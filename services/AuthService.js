const constant = require('../constants/constant');
const hashHelper = require('../packages/hash');
const UserError = require('../errors/UserErrors');

module.exports = (db) => {
  const AuthService = {};

  AuthService.checkCredentials = async (email, password) => {
    const user = await AuthService.findUser(email);

    if (!user) {
      throw new UserError.InvalidEmail();
    }

    if (user.status !== constant.Status.Active) {
      throw new UserError.UserNotActivated();
    }

    if (!hashHelper.bcryptCompareSync(password, user.password)) {
      throw new UserError.InvalidEmailOrPassword();
    }
    return user;
  };

  AuthService.auth = async (email, password) => {
    const user = await AuthService.checkCredentials(email, password);
    return AuthService.generateToken({ email, userId: user.id });
  };

  AuthService.findUser = async (email) => {
    return await db.User.findOne({ where: { email } });
  }

  AuthService.generateToken = ({ email, userId }) => {
    return hashHelper.getJwtToken({ email, userId });
  };

  return AuthService;
};
