const db = require('../../database/connection');
const ProductService = require('../ProductService');

module.exports.init = () => {
	return ProductService(db);
};
