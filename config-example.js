module.exports = {
  port: 3000,
  database: {
    host: '',
    port: '3306',
    database: '',
    user: '',
    password: '',
    dialect: 'mysql' // postgresql, sqlite
  },
  models: `${__dirname}/database/models`,
  log: {
    console: {
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    },
    file: {
      filename: `${__dirname}/App.log`,
      level: 'debug',
      maxsize: 5000000
    }
  },
  auth: {
    secretKey: '',
    tokenExpiresIn: 30000
  }
};
