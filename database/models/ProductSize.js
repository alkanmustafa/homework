module.exports = (sequelize, DataTypes) => {
  const ProductSizeModel = sequelize.define(
    'ProductSize',
    {
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: 'product_id'
      },
      value: {
        type: DataTypes.STRING(255),
        allowNull: false,
        field: 'value'
      }
    }, {
    tableName: 'product_sizes',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    paranoid: true
  }
  );
  ProductSizeModel.associate = (models) => {
    ProductSizeModel.belongsTo(models.Product, {
      foreignKey: 'productId'
    });
  };
  return ProductSizeModel;
};
