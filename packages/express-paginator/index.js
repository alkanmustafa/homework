
const { ValidationError } = require('../../errors/SystemErrors');
const responser = require('../responser');
const { flow, isFunction, remove } = require('lodash');

module.exports.init = (orm) => {

    const builder = require('./' + orm);
    const functions = builder.functions();

    let getFlows = (req, res, next) => {

        let validate = (req) => {

            if (req.query.sortType || req.query.sortBy || req.query.offset || req.query.limit) {

                req.checkQuery({
                    'limit': { notEmpty: true },
                    'offset': { notEmpty: true },
                    'sortBy': { notEmpty: true },
                    'sortType': { notEmpty: true }
                });

                if (req.validationErrors()) {
                    return responser.withError(next, new ValidationError(req.validationErrors(true)));
                }
            }

            return req;

        };

        let parseKey = (options) => {

            let array = options.key.split('_');
            options.functionName = array[array.length - 1];
            remove(array, (item) => item === options.functionName);
            options.key = array.join('_');

            return options;
        };

        let runFunction = (options) => {

            if (!functions[options.functionName] && !isFunction(functions[options.functionName]))
                throw new Error('express-paginator validate: Invalid parameters: operator is null or undefined');

            functions[options.functionName](options);

            return options.criteria;
        }

        return {
            paginateFlow: flow(validate, builder.paginate),
            criteriaFlow: flow(parseKey, runFunction)
        };

    }

    let paginate = (req, res, next) => {

        let criteria = {};
        let includedCriteria = {};

        let flows = getFlows(req, res, next);

        flows.paginateFlow(req);

        for (let key in req.query) {

            if (key.indexOf('.') > 1) {

                let splitKeys = key.split(".");
                includedCriteria[splitKeys[0]] = includedCriteria[splitKeys[0]] || {};

                flows.criteriaFlow({
                    key: splitKeys[1],
                    value: req.query[key],
                    criteria: includedCriteria[splitKeys[0]]
                });

            } else {

                flows.criteriaFlow({
                    key: key,
                    value: req.query[key],
                    criteria: criteria
                });
            }
        }

        req.paginator = req.paginator || {};
        req.paginator.where = criteria;
        req.paginator.includedWhere = includedCriteria;

        builder.set(req);


        return next();
    };

    return {
        paginate: paginate
    }
}