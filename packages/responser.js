module.exports = {
  success: (res, data) => res.status(200).json({data}),
  withError: (next, error) => next(error)
};