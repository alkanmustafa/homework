module.exports = (appPath) => {
  const sequelize = require('./sequelize')(appPath);
  const umzug = require('./umzug')(sequelize, appPath);
  const functions = require('./functions')(umzug, sequelize, appPath);

  return functions;
};
