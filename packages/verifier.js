const { UnauthorizedError } = require('../errors/SystemErrors');
const hashHelper = require('./hash');

module.exports.verifyToken = (req, res, next) => {
  if (!req.headers.authorization) {
    throw new UnauthorizedError();
  }

  try {
    req.currentUser = hashHelper.verifyJwtToken(req.headers.authorization);
    return next();
  } catch (e) {
    return next();
  }
};
