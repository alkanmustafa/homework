const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config');
const logger = require('./packages/logger')(config);

const logStream = logger.stream;

const app = express();


app.use(require('express-request-id')());

app.use((req, res, next) => require('morgan')('combined', {
  stream: logStream({ id: req.id, methodName: 'expressLogging' })
})(req, res, next));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(require('express-validator')());

require('./routes/public')(app);
app.use(require('./packages/verifier').verifyToken);
require('./routes/authenticated')(app);

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(require('./packages/error-handler')(logger.logger));

module.exports = app;
